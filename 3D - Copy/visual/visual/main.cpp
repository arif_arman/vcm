#include "header.h"
#include "myColor.h"
#include "myVector.h"
#include "myGl.h"
#include "myCamera.h"


Camera camera;


void display(void){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(COLOR_BLACK, 0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	//camera set
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(CO(camera.pos),CO(camera.look),CO(camera.upDir));
	glMatrixMode(GL_MODELVIEW);


	//start code

	myAxes();


	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}

void animate(void){
	//codes for any changes in Models, Camera
}

void init(){
	glClearColor(COLOR_BLACK, 0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(70,	1,	0.1,	10000.0);

	// place your init code here
	lightEnable=1;
	

	camera= Camera();
	

}

void specialKeyListener(int key, int x,int y){
	switch(key){
		case GLUT_KEY_DOWN:	
			camera.rotate(DOWN);
			break;
		case GLUT_KEY_UP:
			camera.rotate(UP);
			break;
		case GLUT_KEY_RIGHT:
			camera.rotate(RIGHT);
			break;
		case GLUT_KEY_LEFT:
			camera.rotate(LEFT);
			break;
		case GLUT_KEY_PAGE_UP:
			camera.updateZ(+1);
			break;
		case GLUT_KEY_PAGE_DOWN:
			camera.updateZ(-1);
			break;

		default:
			break;
	}

	camera.balance();


}

void keyboardListener(unsigned char key,int x,int y){
	switch(key)
	{
		case '[': // move FF in the looking direction
			camera.forward(FORWARD);
			break;
		case ']': // move RR in the looking direction
			camera.forward(BACKWARD);
			break;
		case '4':
			camera.updateX(-1);
			break;
		case '6':
			camera.updateX(+1);
			break;
		case '2':
			camera.updateY(+1);
			break;
		case '8':
			camera.updateY(-1);
			break;
		case 'f':
			break;
		case 'l':
			lightEnable=1-lightEnable;
			break;
		case 'p':
			camera.pos.print();
			camera.look.print();
			camera.upDir.print();
			break;
	}

	camera.print();

	//camera.balance();
}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitWindowSize(800, 500);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("Assignment A2");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)
	
	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}
