#define CO(V)	V.x,V.y,V.z
#define CO2(V)	V.x,V.y

int lightEnable;


void setColor(double r,double g,double b)
{
	glColor3f((r/255)*lightEnable,(g/255)*lightEnable,(b/255)*lightEnable);
}


void myAxes()
{
	// along x
	setColor(COLOR_RED);
	glBegin(GL_LINES);
	{
		glVertex3f(100,0,0);
		glVertex3f(0,0,0);	
	}
	glEnd();

	// along y
	setColor(COLOR_GREEN);
	glBegin(GL_LINES);
	{
		glVertex3f(0,100,0);
		glVertex3f(0,0,0);
	}
	glEnd();

	// along z
	setColor(COLOR_BLUE);
	glBegin(GL_LINES);
	{
		glVertex3f(0,0,100);
		glVertex3f(0,0,0);
	}
	glEnd();
}

void myBox(double length,double width,double height)
{
	glPushMatrix();
	{
		glScaled(length,width,height);
		glutSolidCube(1);
	}glPopMatrix();
}

void myLine2f( Vector &u,Vector &v )
{
	glPushMatrix();
	{
		glBegin(GL_LINES);
		{
			glVertex2d(CO2(u));
			glVertex2d(CO2(v));
		}glEnd();
	}glPopMatrix();
}



void myCircle2f(Vector &c, double radius,double ang0=0,double ang1=360)
{
	const double DEG2RAD = PI/180;
	glPushMatrix();
	{
		glTranslated( CO(c) );
		glBegin(GL_LINE_STRIP);
		{
			for (int i=ang0; i <= ang1; i++)
			{
			  double degInRad = i*DEG2RAD;
			  glVertex2f(cos(degInRad)*radius,sin(degInRad)*radius);
			}
		}glEnd();
	}glPopMatrix();
}
