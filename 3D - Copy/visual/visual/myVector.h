
#define EPS 1e-10
// +0.0000000001

#define S(x)	((x)*(x))
#define Z(x)	(fabs(x) < EPS)
#define PI acos(-1)

struct Vector{
	double x,y,z;

	Vector(double x=0,double y=0,double z=0){
		this->x=x;
		this->y=y;
		this->z=z;
	}

	void print()
	{
		printf("%.2lf, %.2lf, %.2lf\n",x,y,z);
	}
	bool isZero(){
		return Z(x) && Z(y) && Z(z);
	}
	double mag(){
		return sqrt(mag2());
	}
	double mag2(){
		return S(x)+S(y)+S(z);
	}
	void normalize(){ //make this unit
		double m = mag();
		x/=m;
		y/=m;
		z/=m;
	}
	Vector unit(){	//this unchanged, make a unit copy of this
		Vector u(x,y,z);
		u.normalize();
		return u;
	}
};


double dotp(Vector a, Vector b){
	return a.x*b.x + a.y*b.y + a.z*b.z;
}
Vector crossp(Vector a, Vector b){
	Vector c;
	c.x = a.y*b.z - a.z*b.y;
	c.y = a.z*b.x - a.x*b.z;
	c.z = a.x*b.y - a.y*b.x;
	return c;
}

Vector operator+(Vector a,Vector b){
	return Vector(a.x+b.x, a.y+b.y, a.z+b.z);	}

Vector operator-(Vector a){
	return Vector(-a.x, -a.y, -a.z);	}

Vector operator-(Vector a,Vector b){
	return Vector(a.x-b.x, a.y-b.y, a.z-b.z);	}

Vector operator*(Vector a,Vector b){
	return crossp(a,b);	}

Vector operator*(Vector a, double b){		// A*2.
	return Vector(a.x*b, a.y*b, a.z*b);	}

Vector operator*(double b, Vector a){		// 2.*A
	return Vector(a.x*b, a.y*b, a.z*b);	}

Vector operator/(Vector a, double b){		// A/2.
	return Vector(a.x/b, a.y/b, a.z/b);	}

double angle(Vector a, Vector b){
	return acos( dotp(a,b) / (a.mag()*b.mag()) );
}

//length of a on b
double projectedLength(Vector a, Vector b){
	return dotp(a, b.unit());
}

//vector component of a on b
Vector projectedVector(Vector a, Vector b){
//	return b.unit() * projectedLength(a,b);
	Vector ub = b.unit();
	return ub * dotp(a, ub);
}

//rotatee=a, axis=s, angle=alpha
Vector rotateVector(Vector a, Vector s, double alpha){	//
	Vector us = s.unit();
	Vector ap = projectedVector(a, us);	//a_parallel
	Vector al = a - ap;					//a_lombo
	Vector b  = us * al;

	//rotated( a_lombo ), X=al, Y=b
	Vector ral = al*cos(alpha) + b*sin(alpha);

	return ap + ral;
}
