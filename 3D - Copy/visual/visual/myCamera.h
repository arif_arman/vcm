
class Camera
{
public:

	Vector pos,look,upDir;
	Vector lookDir;
	
	double radius, xyangle, height,delta, move, zangle;


	Camera()
	{
		pos=Vector( -179,-81,27 );
		look=Vector( -118.381972,-44.184476,21.685836 );
		upDir=Vector( 0,0,1 );


		radius = 200/(2*1.41);
		height = 100*3/4;
		xyangle = PI/2;
		delta = 0.025;
		move = 2;
		zangle = 0;

		lookDir=Vector( cos(xyangle),sin(xyangle),sin(zangle) );// update the looking diraction	



	}

	void print()
	{
		pos.print();
		look.print();
		upDir.print();
	}

	
	void rotate(int direction)
	{
		if		(direction==LEFT)xyangle += delta;
		else if	(direction==RIGHT)xyangle -= delta;
		else if (direction==UP && 2*zangle < PI) zangle += delta*1.5;
		else if (direction==DOWN && 2*zangle > -PI)zangle -= delta*1.5;
		
		lookDir=Vector( cos(xyangle),sin(xyangle),sin(zangle) );// update the looking diraction	
		look=pos + radius*lookDir;//look at a point in that diraction of radious distance 
	}

	void forward(int direction)
	{
		if		(direction== FORWARD && move < 0)move = -move;
		else if	(direction== BACKWARD && move > 0)move = -move;
		
		pos=pos+move*lookDir;//proceed in the looking direction move unit
		look=pos+radius*lookDir;
	}

	void updateX(int dir)
	{
		pos.x+=move*dir;
		look.x+=move*dir;
	}
	void updateY(int dir)
	{
		pos.y+=move*dir;
		look.y+=move*dir;
	}
	void updateZ(int dir)
	{
		pos.z+=move*dir;
		look.z+=move*dir;
	}

	void balance()
	{
		
		pos.x=min(roomX/2,pos.x);
		look.x=min(roomX/2,look.x);

		pos.x=max(-roomX/2,pos.x);
		look.x=max(-roomX/2,look.x);

	
		pos.y=min(roomY/2,pos.y);
		look.y=min(roomY/2,look.y);

		pos.y=max(-roomY/2,pos.y);
		look.y=max(-roomY/2,look.y);

		pos.z=min(roomZ,pos.z);
		look.z=min(roomZ,look.z);

		pos.z=max(0,pos.z);
		look.z=max(0,look.z);
	}

};