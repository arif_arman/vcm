
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <queue>
#include <cassert>



using namespace std;


#include<GL/glut.h>


#include "prevSource.h"
#include "camera.h"
#include "macros.h"
#include "gpc.h"

using namespace std;


GLuint fill_type=GL_FILL;

GLfloat ctrlpoints[4][4][3] = {
	{{-1.5, -1.5, 4.0}, {-0.5, -1.5, 2.0},
	{0.5, -1.5, -1.0}, {1.5, -1.5, 2.0}},
	{{-1.5, -0.5, 1.0}, {-0.5, -0.5, 3.0},
	{0.5, -0.5, 0.0}, {1.5, -0.5, -1.0}},
	{{-1.5, 0.5, 4.0}, {-0.5, 0.5, 0.0},
	{0.5, 0.5, 3.0}, {1.5, 0.5, 4.0}},
	{{-1.5, 1.5, -2.0}, {-0.5, 1.5, -2.0},
	{0.5, 1.5, 0.0}, {1.5, 1.5, -1.0}}
};

#define fac 3
GLfloat texpts[2][2][2] = {{{0.0, 0.0}, {0.0, fac}},
{{fac, 0.0}, {fac, fac}}};

int showPoints = 0;
GLUnurbsObj *theNurb;




//my variable
//for camera control


bool isRotating=false;
bool isFrameRotating=false;
int cameraRadius=100;
int rotatingCameraHeight;

int cameraX;
int cameraY;
int cameraZ;
bool changeCameraPos=false;

int lookAtX=0;
int lookAtY=0;
int lookAtZ=0;
bool changeLookAtPos=false;
///////////////////for ne camera variable
//animation variable
double lookatX,lookatY,lookatZ;
double  cameraAngle, cameraHeight, cameraDelta, cameraMove, cameraVertical;


//float scaleFac=.01;
float scaleFac=1;



Camera cam;
int window;


void animate(){
	glutPostRedisplay();
	//codes for any changes in Models, Camera
}

void draw_axis(){
        glColor3f(1, 1, 1);
        glBegin(GL_LINES);{
                glVertex3f(0, -150, 0);
                glVertex3f(0,  150, 0);
                glVertex3f(-150, 0, 0);
                glVertex3f( 150, 0, 0);
        }glEnd();
        glColor3f(0.5, 0.5, 0.5);
        glBegin(GL_LINES);{
            for(int i = -150; i<=150; i+=10){
                glVertex3f(i, -150, 0);
                glVertex3f(i,  150, 0);
            }
            for(int i = -150; i<=150; i+=10){
                glVertex3f(-150, i, 0);
                glVertex3f(150,  i, 0);
            }
        }glEnd();

}

void init(void)
{
	//for surface mesh
	/*
	glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, 4,
	0, 1, 12, 4, &ctrlpoints[0][0][0]);
	glEnable(GL_MAP2_VERTEX_3);
	glEnable(GL_AUTO_NORMAL);
	glMapGrid2f(20, 0.0, 1.0, 20, 0.0, 1.0);
	initlights();

	*/
	glEnable(GL_MAP2_VERTEX_3);

	//glEnable(GL_DEPTH_TEST);
	///glShadeModel(GL_FLAT);
	//glEnable(GL_AUTO_NORMAL);

	//initlights();
	

	
	cameraX = 300;
	cameraY = 0;
	cameraZ = 30;

	lookatX = 0;
	lookatY = 0;
	lookatZ = 0;

	cameraRadius = 100;
	cameraHeight = 0;
	cameraAngle = 0;
	cameraDelta = 0.025;
	cameraMove = 1.8;
	cameraVertical = 0;
	

	glMatrixMode(GL_PROJECTION);

	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(70,	1,	0.1,	10000.0);
	// gluNurbsCallback(theNurb, GLU_ERROR, nurbsError);
}


////


class Plane
{
public:
	double n1,n2,n3,d0; //plane equation form

	Plane(){}
	Plane( double n1,double n2,double n3,double d0 ){this->n1=n1,this->n2=n2,this->n3=n3,this->d0=d0;}
	Plane (Vector3 p1,Vector3 p2,Vector3 p3)
	{
		Vector3 n=(p3.sub(p1)).crossProd(p2.sub(p1));
		n1=n.x;
		n2=n.y;
		n3=n.z;


		d0=n.dotProd(p1);
	}


	

	// project p on plane wrt c and store in q
	bool caclProjectionOfPoint(    Vector3 C, Vector3 P,Vector3& Q  ) 
	{

		//	if( Z( C.x-P.x ) && Z( C.y-P.y ) && Z( C.z-P.z )  ){Q=P;return true;}

		//	from slide of text

		double d1=n1*C.x+n2*C.y+n3*C.z;
		double d=d0-d1;

		double alp=( n1*( P.x-C.x )+n2*( P.y-C.y )+n3*( P.z-C.z ) );

		if( Z(alp) || alp<0 )return false;

		alp=d/alp;
	
		/*

		// projection matrix

		d+an1 an2 an3 -ad0
		bn1 d+bn2 bn3 -bd0
		cn1 cn2 d+cn3 -cd0
		n1 n2 n3 -d1

		*/

		// point is in form [ x,y,z,w ]


		double pp[4];
		double ap[4]={P.x,P.y,P.z,1};

		double x=P.x,y=P.y,z=P.z;
		double a=C.x,b=C.y,c=C.z;

		double M[4][4]={{d+a*n1, a*n2, a*n3, -a*d0},{b*n1, d+b*n2, b*n3, -b*d0},{c*n1, c*n2 ,d+c*n3 ,-c*d0},{n1 ,n2 ,n3 ,-d1}};

		for(int i=0;i<4;i++ )
		{
			pp[i]=0;
			for(int j=0;j<4;j++ )
			{
				pp[i]+=M[i][j]*ap[j];
			}
		}

		Q.x=pp[0];
		Q.y=pp[1];
		Q.z=pp[2];


		if( Z(pp[3]) )return false;


		Q=Q.div(pp[3]);

		return true;

	}

};

class Box;

class Box
{
public:
	Box(){}

	double x[2],y[2],z[2];
	Vector3 v0,v1;


	// 6 planes we are treating them like a box
	Box *bx,*by,*bz;
	Plane px[2],py[2],pz[2];
	

	// 8 corner point
	Vector3 a,b,c,d;
	Vector3 a1,b1,c1,d1;

	//center of the box
	Vector3 o;

	//length of the side
	double lx,ly,lz;
	
	/* 
		creates the Box from 2 corner points
		finds 8 corner points and center point of Box
	*/
	void init(Vector3 &v0,Vector3 &v1)
	{
		this->v0=v0;
		this->v1=v1;

		x[0]=v0.x;
		y[0]=v0.y;
		z[0]=v0.z;

		x[1]=v1.x;
		y[1]=v1.y;
		z[1]=v1.z;

		/* find 8 corner points from 2 points */
		// say these are the 4 points of bottom surface
		a=Vector3(x[1],y[1],z[1]);
		b=Vector3(x[1],y[0],z[1]);
		c=Vector3(x[0],y[0],z[1]);
		d=Vector3(x[0],y[1],z[1]);

		// these are the 4 points for top surface
		a1=Vector3(x[1],y[1],z[0]);
		b1=Vector3(x[1],y[0],z[0]);
		c1=Vector3(x[0],y[0],z[0]);
		d1=Vector3(x[0],y[1],z[0]);


		// size of sides
		lx=fabs( x[1]-x[0] );
		ly=fabs( y[1]-y[0] );
		lz=fabs( z[1]-z[0] );

		o=v0.add( v1 ).div(2);

	}

	Box(Vector3 v0,Vector3 v1) // tow corner point
	{
		init(v0,v1);
	}

	void scan()
	{
		Vector3 v0,v1;
		v0.scan();
		v1.scan();
		init(v0,v1);
		setPlanes();
	}
	/* takes 2 vectors */
	void fscan(FILE* fp)
	{
		Vector3 v0,v1;
		v0.fscan(fp);
		v1.fscan(fp);
		init(v0,v1);
		setPlanes();
	}

	void setPlanes() // fixing the 6 planes treating them like a box
	{
		/*
			actually the 3D box is used to make 2D box in here
		*/
		bz=new Box[2];
		/* 
			pz denotes to planes parallel to xy plane, z axis borabor 
			px parallel to yz and so on
		*/
		pz[1]=Plane( a,b,c );bz[1]=Box(a,c);
		pz[0]=Plane( a1,b1,c1 );bz[0]=Box(a1,c1);

		bx=new Box[2];
		px[1]=Plane( a,a1,b1 );bx[1]=Box(a,b1);
		px[0]=Plane( d,d1,c1 );bx[0]=Box(d,c1);

		by=new Box[2];
		py[1]=Plane( a,a1,d1 );by[1]=Box(a,d1);
		py[0]=Plane( b,b1,c1 );by[0]=Box(b,c1);

	}

	void print()
	{
		
		cout<<x[0]<<" "<<y[0]<<" "<<z[0]<<endl;
		cout<<x[1]<<" "<<y[1]<<" "<<z[1]<<endl;
	}

	void draw()
	{
		Vector3 c(x[0]+x[1],y[0]+y[1],z[0]+z[1]);
		c=c.div(2);

		double dx=fabs(x[1]-x[0]);
		double dy=fabs(y[1]-y[0]);
		double dz=fabs(z[1]-z[0]);


		Vector3 dv(dx,dy,dz);
		
		
		
		glPushMatrix();
		{
			glTranslated( CO(c) );
			glScaled( CO(dv) );
			glutSolidCube(1);
		}glPopMatrix();
	}


};




//convex hull


Vector V[50007];
int hull[50007];
bool cmp(Vector V1,Vector V2)
{
    int t=Turn(V[0],V1,V2);
    if(!t)
    {
        return (V1-V[0]).mag2()<(V2-V[0]).mag2();
    }
    return t>0;
}
int cHull(int N)
{
    int i,id=0;
    int top=0;
    if(N<=2)
    {
        hull[top++]=0;
        if(N==2)hull[top++]=1;
        return top;
    }
    for(i=1;i<N;i++)
    {
        if(V[i].y==V[id].y)
        {
            if(V[i].x<V[id].x)id=i;
        }
        if(V[i].y<V[id].y)id=i;
    }
    swap(V[0],V[id]);
    sort(V+1,V+N,cmp);
    hull[top++]=0;
    hull[top++]=1;
    for(i=2;i<N;i++)
    {
        while(Turn(V[hull[top-2]],V[hull[top-1]],V[i])<1&&top>1)top--;
        hull[top++]=i;
    }
    return top;
}

//


class polygonAdapter //convert 3d to 2d
{
public:
	
	gpc_polygon poly;
	int dir;
	polygonAdapter(){}

	polygonAdapter( int dir )
	{
		this->dir=dir;
	}

	Vector3 adapt23(int dir,double dist,double x,double y)
	{
		Vector3 ret;
		if( dir==1 )
		{
			ret=Vector3( dist,x,y );
		}

		return ret;
	}

	Vector adapt32(int dir,double x,double y,double z)
	{
		if(dir==1)return Vector(y,z);
	}

	vector<Vector3> adapt23(int dir,double dist,vector<Vector>&v)
	{
		vector<Vector3>ret;
		for(int i=0;i<v.size();i++)ret.push_back( adapt23(dir,dist,v[i].x,v[i].y) );
		return ret;
	}

	// convert 2D to 3D, gpc_polygon to planerPoly
	// planerPoly is basically: 3D point er vector er vector, each contour is stored as a vector of points
	// then each contour is stored in outer vector
	void adapt(int dir,double dist,gpc_polygon& g,vector< vector< Vector3 > >&planerPoly) 
	{
		for( int i=0;i<g.num_contours;i++ )
		{
			planerPoly.resize( planerPoly.size()+1 );
			for(int j=0;j<g.contour[i].num_vertices;j++)
			{
				planerPoly.back().push_back( adapt23( dir,dist,g.contour[i].vertex[j].x,g.contour[i].vertex[j].y ) );
			}
		}
	}

	void adapt( Box &b,gpc_polygon &g) //convert 3D box to 2D rectangle
	{

		g.num_contours=1;
		g.hole=0;
		g.contour= ( gpc_vertex_list *) malloc( g.num_contours*sizeof( gpc_vertex_list ) ) ;
		g.contour[0].num_vertices=4;
		g.contour[0].vertex=( gpc_vertex *)malloc( g.contour[0].num_vertices*sizeof( gpc_vertex ) );
		Vector3 v0=Vector3(b.x[0],b.y[0],b.z[0]);
		Vector3 v1=Vector3(b.x[1],b.y[1],b.z[1]);
		Vector3 dif=v1.sub(v0);
		double x[2],y[2];

		if( Z(dif.x) ) //Box is in the x perp plane
		{
			x[0]=v0.y;
			x[1]=v1.y;

			y[0]=v0.z;
			y[1]=v1.z;
		}

		else if( Z(dif.y) ) //Box is in the y perp plane
		{
			x[0]=v0.z;
			x[1]=v1.z;

			y[0]=v0.x;
			y[1]=v1.x;
		}


		else if( Z(dif.z) ) //Box is in the z perp plane
		{
			x[0]=v0.x;
			x[1]=v1.x;

			y[0]=v0.y;
			y[1]=v1.y;
		}

		g.contour[0].vertex[0].x=x[0];
		g.contour[0].vertex[0].y=y[0];

		g.contour[0].vertex[1].x=x[0];
		g.contour[0].vertex[1].y=y[1];

		g.contour[0].vertex[2].x=x[1];
		g.contour[0].vertex[2].y=y[1];

		g.contour[0].vertex[3].x=x[1];
		g.contour[0].vertex[3].y=y[0];

	}

	void adapt( vector<Vector>&b,gpc_polygon &g )
	{
		g.num_contours=1;
		g.hole=0;
		g.contour= ( gpc_vertex_list *) malloc( g.num_contours*sizeof( gpc_vertex_list ) ) ;
		g.contour[0].num_vertices=b.size();
		g.contour[0].vertex=( gpc_vertex *)malloc( g.contour[0].num_vertices*sizeof( gpc_vertex ) );

		for(int i=0;i<b.size();i++)
		{
			g.contour[0].vertex[i].x=b[i].x;
			g.contour[0].vertex[i].y=b[i].y;
		}

	}

	void polygonMerge( gpc_polygon &p1,gpc_polygon &p2  ) //merge p1,p2 result is in p1
	{
		gpc_polygon temp;
		gpc_polygon_clip( GPC_UNION,&p2,&p1,&temp );
		swap(p1,temp);
	}

	void polygonIntersection( gpc_polygon &p1,gpc_polygon &p2  ) //merge p1,p2 result is in p1
	{
		gpc_polygon temp;
		gpc_polygon_clip( GPC_INT,&p2,&p1,&temp );
		swap(p1,temp);
	}

	bool contains( gpc_polygon &gp1,gpc_polygon &gp2  ) //if p1 contains p2
	{
		myPolygon p1,p2;
		Vector v;

		
		for( int k=0;k<gp1.num_contours;k++ )
		{
			p1.clear();
			int i;
			for(  i=0;i<gp1.contour[k].num_vertices;i++ )p1.P.push_back( Vector( gp1.contour[k].vertex[i].x,gp1.contour[k].vertex[i].y ) );
			for(  i=0;i<gp2.contour[0].num_vertices;i++ )
			{
				v=( Vector( gp2.contour[0].vertex[i].x,gp2.contour[0].vertex[i].y ) );
				if(!p1.PointInPoly(v))break;
			}
			if(i==gp2.contour[0].num_vertices)return true;
		}

		return false;

	}


	void drawGpcPolygon( gpc_polygon &g )
	{
		for(int i=0;i<g.num_contours;i++)
		{
			glBegin(GL_LINE_LOOP);
			for(int  j=0;j<g.contour[i].num_vertices;j++ )
			{
				glVertex3d( g.contour[i].vertex[j].x,g.contour[i].vertex[j].y,0 );
				glVertex3d( g.contour[i].vertex[(j+1)%g.contour[i].num_vertices].x,g.contour[i].vertex[(j+1)%g.contour[i].num_vertices].y,0 );
			}
			glEnd();
		}
	}

	void draw3DPolygon( vector< vector<Vector3> >&g )
	{
		for(int i=0;i<(int)g.size();i++)
		{
			glBegin(GL_LINE_LOOP);
			for(int  j=0;j<(int)g[i].size();j++ )
			{
				glVertex3d( CO(g[i][j]) );
				glVertex3d( CO(g[i][(j+1)%g[i].size()]) );
			}
			glEnd();
		}
	}

	void draw3DPolygon( vector<Vector3>&g )
	{
		glBegin(GL_LINE_LOOP);
		for(int  j=0;j<(int)g.size();j++ )
		{
			glVertex3d( CO(g[j]) );
			glVertex3d( CO(g[(j+1)%g.size()]) );
		}
		glEnd();
	}


	vector<Vector> convexHull(vector<Vector>&P)
	{
		if( P.size()>50004 )
		{
			cout<<"increase the hull array size";
			exit(0);
		}


		for(int i=0;i<P.size();i++)V[i]=P[i];
		int top=cHull( P.size() );

		hull[top]=hull[0];

		vector< Vector > Q;

		for(int i=0;i<top;i++)Q.push_back( V[hull[i]] );

		return Q;



	}


	

}Adapter;




// calculate projection of a Box

bool calcProjectionWrtPoint(Plane plane,Vector3 c,Box tb,Box &pr,int dir=1)
{
	/*
		calculate projection of tb on plane wrt c store in pr
		tb is the a surface of Box T
		we are getting a single Box pr from projRect and assign it's corner points to +INF and -INF
	*/

	vector<Vector>projPoints;

	int i1,j1,k1;

	pr.x[0]=pr.y[0]=pr.z[0]=+INF;
	pr.x[1]=pr.y[1]=pr.z[1]=-INF;
	
	//bool ret=false;
	for( i1=0;i1<2;i1++ )
		for( j1=0;j1<2;j1++ )
			for( k1=0;k1<2;k1++ )
			{
				Vector3 pp;
				
				Vector3 p( tb.x[i1],tb.y[j1],tb.z[k1] );
				/* take all 8 points of tb in p and project in pp */
				if( !plane.caclProjectionOfPoint( c,p,pp ) ) return false;
								
				//ret=true;
				/* min/max nicchi because initially +INF -INF dhora hoyechilo */
				pr.x[0]=min( pr.x[0],pp.x );
				pr.y[0]=min( pr.y[0],pp.y );
				pr.z[0]=min( pr.z[0],pp.z );
				
				pr.x[1]=max( pr.x[1],pp.x );
				pr.y[1]=max( pr.y[1],pp.y );
				pr.z[1]=max( pr.z[1],pp.z );

			}

	return true;

}


bool calcCovexProjectionWrtPoint(Plane plane,Vector3 c,Box tb,gpc_polygon &g,int dir=1)
{
	/*
		calculate projection of tb on plane wrt c store in pr
	*/

	
	
	vector<Vector>projPoints;

	int i1,j1,k1;


	/*

	pr.x[0]=pr.y[0]=pr.z[0]=+INF;
	pr.x[1]=pr.y[1]=pr.z[1]=-INF;
	*/
	//bool ret=false;
	for( i1=0;i1<2;i1++ )
		for( j1=0;j1<2;j1++ )
			for( k1=0;k1<2;k1++ )
			{
				Vector3 pp;
			
				Vector3 p( tb.x[i1],tb.y[j1],tb.z[k1] );

				if( !plane.caclProjectionOfPoint( c,p,pp ) )return false;
								
				//ret=true;

				
				/*
				pr.x[0]=min( pr.x[0],pp.x );
				pr.y[0]=min( pr.y[0],pp.y );
				pr.z[0]=min( pr.z[0],pp.z );

								
				pr.x[1]=max( pr.x[1],pp.x );
				pr.y[1]=max( pr.y[1],pp.y );
				pr.z[1]=max( pr.z[1],pp.z );

				*/

				projPoints.push_back( Adapter.adapt32( 1,pp.x,pp.y,pp.z ) );


			}


	
	
	vector<Vector> pr=Adapter.convexHull( projPoints );
	
	Adapter.adapt(pr,g);



	
	


	return true;


}


bool calcProjectionWrtCube( Plane plane,Box cb,Box pb,Box &pr ) //project a plane wrt a box , projected plane is supposed to be axix alingned
{

	/*
		calculate projection of pb on plane wrt cb store in pr
	*/

	//cb.print();
	//cp.print();

	int i,j,k;
	int i1,j1,k1;




	pr.x[0]=pr.y[0]=pr.z[0]=+INF;
	pr.x[1]=pr.y[1]=pr.z[1]=-INF;

	bool ret=false;

	for( i=0;i<2;i++ )
		for( j=0;j<2;j++ )
			for( k=0;k<2;k++ )
				for( i1=0;i1<2;i1++ )
					for( j1=0;j1<2;j1++ )
						for( k1=0;k1<2;k1++ )
						{
							Vector3 pp;
							Vector3 c( cb.x[i],cb.y[j],cb.z[k] );
							Vector3 p( pb.x[i1],pb.y[j1],pb.z[k1] );

							if( !plane.caclProjectionOfPoint( c,p,pp ) )return false;
								
							ret=true;

							pr.x[0]=min( pr.x[0],pp.x );
							pr.y[0]=min( pr.y[0],pp.y );
							pr.z[0]=min( pr.z[0],pp.z );

								
							pr.x[1]=max( pr.x[1],pp.x );
							pr.y[1]=max( pr.y[1],pp.y );
							pr.z[1]=max( pr.z[1],pp.z );

						}


	return ret;

}


bool calcConvexProjectionWrtCube( Plane plane,Box cb,Box tb,gpc_polygon &global_p,int dir=1 ) //project a plane wrt a box , projected plane is supposed to be axix alingned
{

	/*
		calculate projection of tb on plane wrt cb store in pr
	*/

	//cb.print();
	//cp.print();

	int i,j,k;
	int i1,j1,k1;

	bool ret=false;

	global_p.contour=0;
	bool start=0;
	for( i=0;i<2;i++ )
		for( j=0;j<2;j++ )
			for( k=0;k<2;k++ )	
			{
				Vector3 pp;
				Vector3 c( cb.x[i],cb.y[j],cb.z[k] );
//				Vector3 p( tb.x[i1],tb.y[j1],tb.z[k1] );


				gpc_polygon local_p;

				if( !calcCovexProjectionWrtPoint(plane,c,tb,local_p ) )return false;
				

//				calcCovexProjectionWrtPoint(plane,c,tb,local_p );
//				Adapter.adapt( v,local_p );

				if(!start)
				{
					global_p=local_p;
					start=1;
					continue;
				}

				Adapter.polygonIntersection( global_p,local_p );

			}

	return true;
}

bool commonBox( Box &a,Box &b,Box &c )
{
	// 0 => upper surface, 1 => lower surface
	// there are two boxes, v0 contains the max point of lower surface among two boxes
	// v1 contains min point of upper surface among two boxes
	Vector3 v0(max( a.x[0],b.x[0] ),max( a.y[0],b.y[0] ),max( a.z[0],b.z[0] ));
	Vector3 v1(min( a.x[1],b.x[1] ),min( a.y[1],b.y[1] ),min( a.z[1],b.z[1] ));
	
	c=Box(v0,v1);

	if( c.x[1]-c.x[0]<0  )return false;
	if( c.y[1]-c.y[0]<0  )return false;
	if( c.z[1]-c.z[0]<0  )return false;
	
	return true;
}


#include<queue>
vector<Box>obstacles;
Box target;


class CompareLoX
{
	public:
    bool operator() (int b1, int b2)
    {
        return obstacles[b1].x[1]>obstacles[b2].x[1];
    }
};

class CompareHiX
{
	public:
    bool operator() (int b1, int b2)
    {
        return obstacles[b1].x[0]<obstacles[b2].x[0];
    }
};

class CompareLoY
{
	public:
    bool operator() (int b1, int b2)
    {
        return obstacles[b1].y[1]>obstacles[b2].y[1];
    }
};

class CompareHiY
{
	public:
    bool operator() (int b1, int b2)
    {
        return obstacles[b1].y[0]<obstacles[b2].y[0];
    }
};
class CompareLoZ
{
	public:
    bool operator() (int b1, int b2)
    {
        return obstacles[b1].z[1]>obstacles[b2].z[1];
    }
};

class CompareHiZ
{
	public:
    bool operator() (int b1, int b2)
    {
        return obstacles[b1].z[0]<obstacles[b2].z[0];
    }
};

/* array of integer vector, stores id of obstacles in particular direction */
vector<int>dirObstacleId[6]; // important obstacles in the specific direction , only reserves the index
	// total 6 ta direction
#define NDISTESEG 15 
vector<double>segDist;

/* center of Box theke koto dure ase Segment */
void calcSegmentDistance()
{
	segDist=vector<double>(NDISTESEG); // this will be calculated based on properties of eye
	segDist[0]=target.lx/2+1;
	for( int i=1;i<NDISTESEG;i++ ) segDist[i]=segDist[i-1]+5;	// for debug purpose only

}

Box dirBox[6];
Vector3 unitDir[6];	// contains 6 unit dir vector
Box projRect[6][NDISTESEG]; // projection plane
int totSeg[6];	// stores the no. of segments where all obstacles are considered
gpc_polygon gProjPolys[6][NDISTESEG]; // projected polygon

vector<vector<Vector3> > ProjPolys[6][NDISTESEG]; // 3D planer polygons

// ekta problem ase...
// coordinate system k target cube er center e nia jabo

void calcProjectionPlanes()
{
	/* creates temp array that holds 6 planes of Target stored as Box */
	Box btemp[]={ target.bx[0],target.bx[1],target.by[0],target.by[1],target.bz[0],target.bz[1] };
	for(int i=0;i<6;i++) dirBox[i]=btemp[i];
	/* adds the unit vectors */
	Vector3 tempv[]={ Vector3(-1,0,0),Vector3(+1,0,0),Vector3(0,-1,0),Vector3(0,+1,0),Vector3(0,0,-1),Vector3(0,0,+1) };
	for(int i=0;i<6;i++) unitDir[i]=tempv[i];

	/* translates the projection plane to center of cube */
	double transfactor[]={ target.o.x,target.o.x,target.o.y,target.o.y,target.o.z,target.o.z }; // we have to translate the plane centering at cube center
	Plane projPlane;
	/* outer loop: number of segments => koto dure dure projection plane create hobe */
	for( int i=0;i<NDISTESEG;i++ )
	{
		/* 6ta direction, by unit vector*/
		for(int j=0;j<6;j++)
		{
			double d;
			/* ax+by+cz+d = 0, plane equation, compute d */
			/* alternatively selects +ve and -ve axis */
			if(j&1) d=fabs(segDist[i]+transfactor[j]);
			else d=fabs(-segDist[i]+transfactor[j]);
				
			projPlane=Plane( unitDir[j].x,unitDir[j].y,unitDir[j].z , d  ); 
			/* projection of dirbox[j] (some surface of T) on plane wrt o and store in projRect */
			calcProjectionWrtPoint( projPlane,target.o,dirBox[j],projRect[j][i] );
		}
	}
}

void update( int dir,int curSeg )
{
	// id holds all ids of important obstacles in dir
	vector<int>&id=dirObstacleId[ dir ]; // takes reference of vector of dir, in this case dir = 1
	gProjPolys[dir][curSeg].num_contours=0;

	for( int i=0;i<id.size();i++ )
	{
		gpc_polygon local_p;
		// equation of projection plane
		Plane projPlane=Plane( +1,0,0,segDist[curSeg]+target.o.x );
		// id[i] te index ase important obstacle er
		// obstacles[id[i]] diye obstacle ta access kori
		calcConvexProjectionWrtCube(projPlane, target , obstacles[id[i]] ,local_p);
		// local_p holds projection of id[i] th obstacle, adapter merges it with gProjPolys
		Adapter.polygonMerge( gProjPolys[dir][curSeg],local_p );
	}

}

Vector3 targetPoints[NDISTESEG][NDISTESEG][NDISTESEG];
gpc_polygon gVcmProjPolys[6][NDISTESEG][NDISTESEG][NDISTESEG][NDISTESEG];
vector< vector<Vector3> > vcmProjPolys[6][NDISTESEG][NDISTESEG][NDISTESEG][NDISTESEG]; //3D planer polygons
// dir,curseg,point id on target

/* finds projection of obstacles wrt some point on T on current segment projPlane */
void updateVcm( vector<int>&obstacleWrtPoint,int dir,int curSeg,int i1,int j1,int k1 )
{
	vector<int>&id=obstacleWrtPoint;
	Vector3 c=targetPoints[i1][j1][k1];
	gpc_polygon &g=gVcmProjPolys[dir][curSeg][i1][j1][k1];
		
	g.num_contours=0;

	for( int i=0;i<id.size();i++ )
	{
		gpc_polygon local_p;
		Plane projPlane=Plane( +1,0,0,segDist[curSeg]+c.x );
		calcCovexProjectionWrtPoint(projPlane, c , obstacles[id[i]] ,local_p);
		Adapter.polygonMerge( g,local_p );
	}
}



void reductionInNegX(int dir) //dir=1
{
	priority_queue<int, vector<int>, CompareHiX> pq;
	for(int i=0;i<obstacles.size();i++)if( obstacles[i].x[dir]<=target.x[dir] )pq.push(i); //change here ofr direction

	cout<<"no of obstacles in negative x direction"<<endl;
	cout<<pq.size()<<endl;
	
	int &curSeg=totSeg[dir];
	curSeg=0;

	while(!pq.empty())
	{
	 	int u=pq.top();	// index of considering obstacle
		pq.pop();

		while( curSeg<NDISTESEG && target.x[dir]-segDist[curSeg]>=obstacles[ u ].x[ dir ] ) //change here
		{
			curSeg++;
			update( dir,curSeg ); // eikhane optimize korbo aro...
			Adapter.adapt( dir,target.x[dir]-segDist[curSeg],gProjPolys[dir][curSeg],ProjPolys[dir][curSeg] ); //change here
			
		}
		
		
		Box projPoly;
		
		Plane projPlane=Plane( -1,0,0, fabs(-segDist[curSeg]+target.o.x) ); //change here for direction
		Box boundRect=projRect[dir][curSeg];
		if(!calcProjectionWrtCube(projPlane, target , obstacles[u] ,projPoly))
		{
			printf("discarding id %d because of near distance\n",u);
			continue;
		}
		Box projPoly1;
		if( !commonBox( projPoly,boundRect,projPoly1 ) )
		{
			printf("discarding id %d because of because out of the rojection rectangle\n",u);
			continue;
		}
		gpc_polygon gProjPoly;
		
		Adapter.adapt( projPoly1,gProjPoly );
		
		if(Adapter.contains( gProjPolys[dir][curSeg],gProjPoly ))
		{
			printf("discarding id %d view blocked by other obstacles\n",u);
			continue;
		}
		
		dirObstacleId[dir].push_back( u );
	}
}

void reductionInPosX1(int dir) //dir=1
{
	priority_queue<int, vector<int>, CompareLoX> pq;
	for(int i=0;i<obstacles.size();i++)if( obstacles[i].x[dir]>=target.x[dir] )pq.push(i); //change here ofr direction
	
	int &curSeg=totSeg[dir];
	curSeg=0;


	while(!pq.empty())
	{
	 	int u=pq.top();
		pq.pop();

		
		
		while( curSeg<NDISTESEG && target.x[dir]+segDist[curSeg]<=obstacles[ u ].x[ dir ] )
		{
			curSeg++;
			update( dir,curSeg ); // eikhane optimize korbo aro...
			Adapter.adapt( dir,target.x[dir]+segDist[curSeg],gProjPolys[dir][curSeg],ProjPolys[dir][curSeg] );
			
		}
		
		
		Box projPoly;
		Plane projPlane=Plane( +1,0,0,segDist[curSeg]+target.o.x ); //change here for direction
		Box boundRect=projRect[dir][curSeg];
		if(!calcProjectionWrtCube(projPlane, target , obstacles[u] ,projPoly))
		{
			printf("discarding id %d because of near distance\n",u);
			continue;
		}
		Box projPoly1;
		if( !commonBox( projPoly,boundRect,projPoly1 ) )
		{
			printf("discarding id %d because of because out of the rojection rectangle\n",u);
			continue;
		}
		gpc_polygon gProjPoly;
		
		Adapter.adapt( projPoly1,gProjPoly );
		
		if(Adapter.contains( gProjPolys[dir][curSeg],gProjPoly ))
		{
			printf("discarding id %d view blocked by other obstacles\n",u);
			continue;
		}
		
		dirObstacleId[dir].push_back( u );
	}
}


void reductionInPosX(int dir) //dir=1 => +ve X axis ?
{
	/* contains the index of obstacle ? */
	priority_queue<int, vector<int>, CompareLoX> pq;
	/* 
		if obstacle's bottom point is at some positive distance along X axis than Target's bottom point then need to consider it
		so push in pq
	*/
	for(int i=0;i<obstacles.size();i++)if( obstacles[i].x[dir]>=target.x[dir] )pq.push(i); //change here for direction
	
	int &curSeg=totSeg[dir];	// particular direction e koto number segment e achi
	curSeg=0;

	while(!pq.empty())
	{
	 	int u=pq.top();	// index of obstacle
		pq.pop();

		/* 
			iterates as long as all segments are not covered and
			if obstacle at index u falls between target and plane at segDist (i.e. has effect on visibility)
		*/
		while( curSeg<NDISTESEG && target.x[dir]+segDist[curSeg]<=obstacles[ u ].x[ dir ] )
		{
			curSeg++;
			// updates projected polygon for curseg in gProjPolys
			update( dir,curSeg ); // eikhane optimize korbo aro...
			Adapter.adapt( dir,target.x[dir]+segDist[curSeg],gProjPolys[dir][curSeg],ProjPolys[dir][curSeg] );
		}
		/* for the following code, curSeg is the segment that is behind obstacles[u] along X axis */
		Box projPoly;	// contains projection of obstacles[u] wrt target on projPlane
		Plane projPlane=Plane( +1,0,0,segDist[curSeg]+target.o.x ); //change here for direction
		Box boundRect=projRect[dir][curSeg];	// projRect holds projection of some surface of T at dir on curSeg
		if(!calcProjectionWrtCube(projPlane, target , obstacles[u] ,projPoly))
		{
			printf("discarding id %d because of near distance\n",u);
			continue;
		}
		Box projPoly1;
		if( !commonBox( projPoly,boundRect,projPoly1 ) )
		{
			printf("discarding id %d because of because out of the rejection rectangle\n",u);
			continue;
		}
		gpc_polygon gProjPoly;

		calcConvexProjectionWrtCube( projPlane,target,obstacles[u],gProjPoly );
		/*
			gProjPolys[dir][curSeg] contains all projected polygons of important obstacles
			now if gProjPoly eder kono ektar majhe contained thake that means ager considered obstacles 
			have shadowed this obstacle => therefore can be discarded
		*/
		if(Adapter.contains( gProjPolys[dir][curSeg],gProjPoly ))
		{
			printf("discarding id %d view blocked by other obstacles\n",u);
			continue;
		}
		/* otherwise this obstacle still have some effect  */
		dirObstacleId[dir].push_back( u );
	}
}




void reductionInPosY(int dir) //dir=3
{
	priority_queue<int, vector<int>, CompareLoX> pq;
	for(int i=0;i<obstacles.size();i++)if( obstacles[i].y[dir]>=target.y[dir] )pq.push(i); //change here ofr direction
	
	int &curSeg=totSeg[dir];
	curSeg=0;


	while(!pq.empty())
	{
	 	int u=pq.top();
		pq.pop();

		/*
		
		while( curSeg<NDISTESEG && target.y[dir]+segDist[curSeg]<=obstacles[ u ].y[ dir ] )
		{
			curSeg++;
			update( dir,curSeg ); // eikhane optimize korbo aro...
			Adapter.adapt( dir,target.y[dir]+segDist[curSeg],gProjPolys[dir][curSeg],ProjPolys[dir][curSeg] );
			
		}
		
		
		Box projPoly;
		Plane projPlane=Plane( +1,0,0,segDist[curSeg]+target.o.y ); //change here for direction
		Box boundRect=projRect[dir][curSeg];
		if(!calcProjectionWrtCube(projPlane, target , obstacles[u] ,projPoly))
		{
			printf("discarding id %d because of near distance\n",u);
			continue;
		}
		Box projPoly1;
		if( !commonBox( projPoly,boundRect,projPoly1 ) )
		{
			printf("discarding id %d because of because out of the rojection rectangle\n",u);
			continue;
		}
		gpc_polygon gProjPoly;
		
		Adapter.adapt( projPoly1,gProjPoly );
		
		if(Adapter.contains( gProjPolys[dir][curSeg],gProjPoly ))
		{
			printf("discarding id %d view blocked by other obstacles\n",u);
			continue;
		}*/
		
		dirObstacleId[dir].push_back( u );
	}
}


void reductionOfObstacle(  )
{
	// making 6 partition not disjoint of the obstacles
	
	// do for positive x direction
	
	reductionInPosX(1);
	//reductionInPosY(3);



	

	/*for(int i=0;i<obstacles.size();i++)if( obstacles[i].x[0]<=target.x[0] )dirObstacleId[1].push_back(i);
	sort(dirObstacleId[1].begin(),dirObstacleId[1].end(),cmphix);
	for(int i=0;i<obstacles.size();i++)if( obstacles[i].y[1]>=target.y[1] )dirObstacleId[2].push_back(i);
	sort(dirObstacleId[2].begin(),dirObstacleId[2].end(),cmploy);
	for(int i=0;i<obstacles.size();i++)if( obstacles[i].y[0]<=target.y[0] )dirObstacleId[3].push_back(i);
	sort(dirObstacleId[3].begin(),dirObstacleId[3].end(),cmphiy);
	for(int i=0;i<obstacles.size();i++)if( obstacles[i].z[1]>=target.z[1] )dirObstacleId[4].push_back(i);
	sort(dirObstacleId[4].begin(),dirObstacleId[4].end(),cmploz);
	for(int i=0;i<obstacles.size();i++)if( obstacles[i].z[0]<=target.z[0] )dirObstacleId[5].push_back(i);
	sort(dirObstacleId[5].begin(),dirObstacleId[5].end(),cmphiz);	
	

	for(int dir=0;dir<1;dir++)
	{
		vector<int>&Id=dirObstacleId[dir];
		
		for(int i=0;i<Id.size();i++)
		{
			


		}
	}
	*/


}

void vcmPhase1()
{
//	reductionOfObstacle();
}

void debug0()
{
	Plane p(0,1,0,2);
	Vector3 c(0,0,0);



	Vector3 v(0,0,2);

	Vector3 u;

	cout<<p.caclProjectionOfPoint(c,v,u)<<endl;
}

int nObstacle;

void input()
{
	FILE* fp=fopen("sample.txt","r");

	target.fscan(fp);
	fscanf(fp,"%d",&nObstacle);

	obstacles=vector<Box>(nObstacle);

	for(int i=0;i<(int)obstacles.size();i++)
	{
		obstacles[i].fscan(fp);
	}

	fclose(fp);
}

Vector3 rColor[100];

bool showDistRec=1,showProjPoly=1,showObstacle=1,showAll=1;

void debug1()
{

	glColor3ub(255,193,37);
	//SET_GOLD_FRONT
	target.draw();
	
	

	for(int dir=0;dir<=0;dir++)
	{
		int k=2*dir+1;
		for(int i=0;i<dirObstacleId[k].size();i++)
		{
		
			glColor3ub( rColor[dirObstacleId[k][i]].x,rColor[dirObstacleId[k][i]].y,rColor[dirObstacleId[k][i]].z );
			if(showObstacle)obstacles[dirObstacleId[k][i]].draw();
		}
	}


	for(int dir=0;dir<=0;dir++)
	{
		int k=2*dir+1;
		for(int i=0;i<=totSeg[k];i++)
		{
			glColor3ub(0,255,255);
			if(showDistRec)projRect[k][i].draw();
			glColor3ub(0,0,0);
			if(i<totSeg[k])if(showProjPoly)
			{
//				Adapter.drawGpcPolygon( gProjPolys[k][i+1] );
				Adapter.draw3DPolygon( ProjPolys[k][i+1] );
			}
		}
	}


	if( showAll && !showObstacle )for( int i=0;i<obstacles.size();i++ )
	{
		glColor3ub( rColor[i].x,rColor[i].y,rColor[i].z );
		obstacles[i].draw();
	}
	
	//Adapter.drawGpcPolygon( gProjPolys[1][totSeg[1]] );

	Adapter.drawGpcPolygon( gVcmProjPolys[1][totSeg[1]][0][0][0] );
	

	return;


}
/* generates 100 random colors and stores in array */
void generateRandCol()
{
	for(int i=0;i<100;i++)rColor[i]=Vector3( rand()%255,rand()%255,rand()%255 );
}

/* similar to reductionInPosX function */
void generateMap( int dir,int i1,int j1,int k1 )
{
	cout << "Entering generate map" << endl;
	Vector3 tp=targetPoints[i1][j1][k1];
	vector< int >&obstacleId=dirObstacleId[dir];	// ids of important obstacles

	int curSeg=0;

	vector< vector<Vector3> > &p=vcmProjPolys[dir][curSeg][i1][j1][k1];
	gpc_polygon &g=gVcmProjPolys[dir][curSeg][i1][j1][k1];
	/* stores id of set of important obstacles */
	vector<int>obstacleWrtPoint;

	for(int i=0;i<obstacleId.size();i++)
	{
		int u=obstacleId[i];
		
		while( curSeg<NDISTESEG && tp.x+segDist[curSeg]<=obstacles[ u ].x[ dir ] )
		{
			curSeg++;
			updateVcm( obstacleWrtPoint,dir,curSeg,i1,j1,k1 );	// eikhane optimize korbo aro...
			Adapter.adapt( dir,tp.x+segDist[curSeg],g,p );		// 2D to 3D adapt
			
		}
		
		Box projPoly;
		Plane projPlane=Plane( +1,0,0,segDist[curSeg]+target.o.x ); //change here for direction
		Box boundRect=projRect[dir][curSeg];
		if(!calcProjectionWrtCube(projPlane, target , obstacles[u] ,projPoly))
		{
			printf("discarding id %d because of near distance\n",u);
			continue;
		}
		Box projPoly1;
		if( !commonBox( projPoly,boundRect,projPoly1 ) )
		{
			printf("discarding id %d because of because out of the rojection rectangle\n",u);
			continue;
		}

		gpc_polygon gProjPoly;

		calcConvexProjectionWrtCube( projPlane,target,obstacles[u],gProjPoly );

		if(Adapter.contains( g,gProjPoly ))
		{
			printf("discarding id %d view blocked by other obstacles\n",u);
			continue;
		}
		
		obstacleWrtPoint.push_back( u );
	}

	cout << "Leaving generate map" << endl;
}

void run()
{
	generateRandCol();
	input();
	
	calcSegmentDistance();
	calcProjectionPlanes();
	reductionOfObstacle(  );

	int dir=1;
	for(int i=0;i<totSeg[1];i++ )
	{
		// stores 2D gpc_polygons as 3D ProjPolys ??
		Adapter.adapt(dir,segDist[i]+target.x[1],gProjPolys[dir][i],ProjPolys[dir][i]);
	}

	//obstacles[ dirObstacleId[1][0] ].print();
	// number of important obstacles in dir
	cout<<dirObstacleId[1].size()<<endl;

	// now we have reduced obstacle set

	targetPoints[0][0][0]=Vector3(target.x[1],target.y[1],target.z[1]);

	generateMap( 1,0,0,0 );

	
}


///


void display(void)
{
	int i, j;
	glClearColor(1,1,1, 0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

//	cameraRadius=abs(cameraX-lookAtX);
//	gluLookAt(cameraRadius*cos(cameraAngle), cameraRadius*sin(cameraAngle), cameraZ,		lookAtX,lookAtY,lookAtZ,		0,0,1);
	

	gluLookAt(  CO(cam.eye) , CO( cam.eye.add(  cam.n ) ) , CO(cam.v) );

	
	//gluLookAt(  CO(cam.eye) , 0,0,0 , CO(cam.v) );

	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);
	/*
	glPushMatrix();
	glColor3ub(0,200,0);
	glPointSize(5.0);
	glScalef(10,10,10);
	for (j = 0; j <= 8; j++) {
	glBegin(GL_POINTS);//GL_LINE_STRIP

	for (i = 0; i <= 30; i++)
	glEvalCoord2f((GLfloat)i/30.0, (GLfloat)j/8.0);
	glEnd();
	glBegin(GL_LINE_STRIP);
	for (i = 0; i <= 30; i++)
	glEvalCoord2f((GLfloat)j/8.0, (GLfloat)i/30.0);
	glEnd();
	}
	glPopMatrix();
	*/

    


	//draw_axis();
	debug1();






	
	glFlush();
	glutSwapBuffers();
}
/*
void setLookAt(string direction)
{
	if		(direction.compare("left") == 0)	cameraAngle += cameraDelta;
	else if	(direction.compare("right") == 0)	cameraAngle -= cameraDelta;
	else if (direction.compare("up") == 0 && cameraVertical < 1)		cameraVertical += cameraDelta*1.5;
	else if (direction.compare("down") == 0  && cameraVertical > -1)	cameraVertical -= cameraDelta*1.5;
	lookatX = cameraX + cameraRadius * cos(cameraAngle);
	lookatY = cameraY + cameraRadius * sin(cameraAngle);
	lookatZ = cameraZ + cameraRadius * sin(cameraVertical);
}

void setMovePosForward(string direction)
{
	if		(direction.compare("forward") == 0 && cameraMove < 0)	cameraMove = -cameraMove;
	else if	(direction.compare("backward") == 0 && cameraMove > 0)	cameraMove = -cameraMove;

	cameraX = cameraX + cameraMove * cos(cameraAngle);
	cameraY = cameraY + cameraMove * sin(cameraAngle);

	lookatX = cameraX + cameraRadius * cos(cameraAngle);
	lookatY = cameraY + cameraRadius * sin(cameraAngle);
}
*/
void keyboardListener(unsigned char key, int x,int y){


	double dd=.2;

	switch (key)
        {
                // camera slide controls
               
					/*
                case 'w': cam.slide(0, 0, -ds); break; // slide camera forward
                case 's': cam.slide(0, 0, ds); break; // slide camera back
                case 'q': cam.slide(0, ds, 0); break; // slide camera up
                case 'z': cam.slide(0, -ds, 0); break; // slide camera down
                case 'a': cam.slide(-ds, 0, 0); break; // slide camera left
                case 'd': cam.slide(ds, 0, 0); break; // slide camera right
					*/

			/*
                // camera pitch controls
                case 'i': cam.rotate(Vector3(1, 0, 0), -1); break;
                case 'I': cam.rotate(Vector3(1, 0, 0), 1); break;
                // camera yaw controls
                case 'j': cam.rotate(Vector3(0, 1, 0), -1); break;
                case 'J': cam.rotate(Vector3(0, 1, 0), 1); break;
                // camera roll controls
                case 'k': cam.rotate(Vector3(0, 0, 1), 1); break;
                case 'K': cam.rotate(Vector3(0, 0, 1), -1); break;

					*/
					

	case 'r':	showDistRec^=1;break;
	case 'p':	showProjPoly^=1;break;
	case 'o':	showObstacle^=1;break;
		case 'a':	showAll^=1;break;

				// camera pitch controls
                case 'i': cam.pitch(-dd); break;
                case 'I': cam.pitch(dd); break;
                // camera yaw controls
                case 'j': cam.yaw(-dd); break;
                case 'J': cam.yaw(dd); break;
                // camera roll controls
                case 'k': cam.roll(1); break;
                case 'K': cam.roll(-1); break;

                case ESCAPE: glutDestroyWindow(window); exit(0); break;
        }
        glutPostRedisplay(); // draws it again
}

void specialKeyListener(int key, int x,int y){



	
	double ds=.5;


	switch(key)
	{
		case GLUT_KEY_DOWN: cam.slide(0, 0, -ds); break; // slide camera forward
		case GLUT_KEY_UP: cam.slide(0, 0, ds); break; // slide camera back
		case GLUT_KEY_PAGE_UP: cam.slide(0, ds, 0); break; // slide camera up
		case GLUT_KEY_PAGE_DOWN: cam.slide(0, -ds, 0); break; // slide camera down
		case GLUT_KEY_LEFT: cam.slide(ds, 0, 0); break; // slide camera left
		case GLUT_KEY_RIGHT: cam.slide(-ds, 0, 0); break; // slide camera right

				
	}
	


}


// this is the mouse event handler
// the x and y parameters are the mouse coordinates when the button was pressed
void mouse(int button, int state, int x, int y)
{



	if(state==GLUT_DOWN)
	{
		cam.eye.print();
		cam.u.print();
		cam.n.print();
		cam.v.print();
	}

        //cout << x; // for debugging
	/*
        switch(button)
        {
        case GLUT_LEFT_BUTTON:
                if (state == GLUT_DOWN)
                {
                        cam.rotate(Vector3(0, 0, 1), -1); break;
                }
                break;
        case GLUT_RIGHT_BUTTON:
                if (state == GLUT_DOWN)
                {
                        cam.rotate(Vector3(0, 0, 1), 1); break;
                }
                break;
        default:
                break;
        }*/
}

void mouseMove(int x, int y)
{

	return;
	/*
        mousePosX = x - mousePosX;
        mousePosY = y - mousePosY;

        cam.rotate(Vector3(0, 1, 0), mousePosX);
        cam.rotate(Vector3(1, 0, 0), mousePosY); // airplane controls :)

        mousePosX = x;
        mousePosY = y;*/
}
/*
int main()
{
	debug0();
	return 0;
}
*/


bool debug2()
{
	myPolygon p1,p2;


	p1.P.push_back( Vector(0,0) );
	p1.P.push_back( Vector(2,0) );
	p1.P.push_back( Vector(2,2) );
	p1.P.push_back( Vector(0,2) );

	p2.P.push_back( Vector(0,0) );
	p2.P.push_back( Vector(3,0) );
	p2.P.push_back( Vector(3,3) );
	p2.P.push_back( Vector(0,3) );

	int i;
	for(  i=0;i<p2.P.size();i++ )
	{
		if( !p1.PointInPoly(p2.P[i]) )break;
	}
	return i==p2.P.size();



}

int main(int argc, char **argv){
	
//	cout<<debug2();
	//while(1);
	run();
	//debug1();

	glutInit(&argc,argv);
	glutInitWindowSize(1000, 500);

	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color
	//	 glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("My OpenGL Program");

	init();
	printf("OpenGL version supported by this platform (%s): \n", glGetString(GL_VERSION));

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)


	//ADD keyboard listeners:
	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);


	glutMouseFunc(mouse);
    glutPassiveMotionFunc(mouseMove);


	glutMainLoop();		//The main loop of OpenGL

	return 0;
}
