#include<stdio.h>
class Vector3
{
public:
	double x;
	double y;
	double z;
    Vector3();
    Vector3(double,double,double);
    Vector3(Vector3&);
	void print();
	void scan();
	void fscan(FILE* fp);
    void set(Vector3);
    void set(double,double,double);
    void setX(double);
    void setY(double);
    void setZ(double);
    double getX();
    double getY();
    double getZ();
    double dotProd(Vector3);
    Vector3 crossProd(Vector3);
	Vector3 add(Vector3);
	Vector3 sub(Vector3);
	Vector3 mul(double scale);
	Vector3 div(double scale);
    void normalize();            
	double mag2();
	bool operator<( const Vector3& v );
};
