#include <cmath>
#include "Vector3.h"
#include "macros.h"

Vector3::Vector3()
{
        x = 0.0;
        y = 0.0;
        z = 0.0;
}

Vector3::Vector3(double dx,double dy,double dz)
{
        x = dx;
        y = dy;
        z = dz;
}

Vector3::Vector3(Vector3& vector)
{
        x = vector.getX();
        y = vector.getY();
        z = vector.getZ();
}

void Vector3::set(Vector3 vector)
{
        x = vector.getX();
        y = vector.getY();
        z = vector.getZ();
}

void Vector3::set(double dx,double dy,double dz)
{
        x = dx;
        y = dy;
        z = dz;
}

void Vector3::setX(double dx)
{
        x = dx;
}

void Vector3::setY(double dy)
{
        y = dy;
}

void Vector3::setZ(double dz)
{
        z = dz;
}

double Vector3::getX()
{
        return x;
}

double Vector3::getY()
{
        return y;
}

double Vector3::getZ()
{
        return z;
}

double Vector3::dotProd(Vector3 vector)
{
        double dot = (x * vector.getX()) + (y * vector.getY()) +
                                (z * vector.getZ());
        return dot;
}

Vector3 Vector3::crossProd(Vector3 vector)
{
        double i = (y * vector.getZ()) - (z * vector.getY());
        double j = (x * vector.getZ()) - (z * vector.getX());
        double k = (x * vector.getY()) - (y * vector.getX());

        Vector3 cross(i,-j,k);
        return cross;
}


Vector3 Vector3::add(Vector3 vector)
{
	return Vector3( x+vector.x , y+vector.y,z+vector.z );
}

Vector3 Vector3::sub(Vector3 vector)
{
	return Vector3( x-vector.x , y-vector.y,z-vector.z );
}

Vector3 Vector3::mul(double scale)
{
	return Vector3( x*scale , y*scale,z*scale );
}


Vector3 Vector3::div(double scale)
{
	return Vector3( x/scale , y/scale,z/scale );
}


void Vector3::normalize()
{
        double length = sqrt((x*x)+(y*y)+(z*z));

        x = x / length;
        y = y / length;
        z = z / length;
}

double Vector3::mag2()
{
	return x*x+y*y+z*z;
}


void Vector3::print()
{
	printf("%.2lf %.2lf %.2lf  %.2lf\n",x,y,z,mag2());

}

void Vector3:: scan()
{
	scanf("%2lf %2lf %2lf",&x,&y,&z);
}

/* takes input from file */
void Vector3:: fscan(FILE* fp)
{
	fscanf(fp,"%2lf %2lf %2lf",&x,&y,&z);
}


bool Vector3:: operator<( const Vector3& v )
{
	if( Z(v.x-x) && Z(v.y-y) )return v.z<z;
	if( Z(v.x-x)  )return v.y<y;
	return v.x<x;
}